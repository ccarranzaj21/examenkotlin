package com.example.examenkotlin

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class RectanguloActividad : AppCompatActivity() {
    private lateinit var editTextLength: EditText
    private lateinit var editTextWidth: EditText
    private lateinit var buttonCalculate: Button
    private lateinit var textViewResult: TextView

    private lateinit var btnLimpiar: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rectangulo_actividad)

        editTextLength = findViewById(R.id.editTextLength)
        editTextWidth = findViewById(R.id.editTextWidth)
        buttonCalculate = findViewById(R.id.buttonCalculate)
        textViewResult = findViewById(R.id.textViewResult)

        buttonCalculate.setOnClickListener {
            calculateAreaAndPerimeter()
        }



    }

    private fun calculateAreaAndPerimeter() {

        if (editTextWidth.text.isNotEmpty() && editTextWidth.text.isNotEmpty()){

            val length = editTextLength.text.toString().toFloat()
            val width = editTextWidth.text.toString().toFloat()
            val area = length * width
            val perimetro = 2 * (length + width)
            val result = "Area: $area\nPerimetro: $perimetro"

            textViewResult.text = result
        } else {
            Toast.makeText(this, "Por favor, rellene todos los campos.", Toast.LENGTH_SHORT).show()
        }



    }





}